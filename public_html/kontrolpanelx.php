<?php
  include("model/fonksiyonlar.php");
  $hata = 0;
  if ($_POST) {
      if($_POST['k_adi']==$vt->admin_adi()&&$_POST['sifre']==$vt->admin_sifre()){
          //Eğer giriş başarılı değil ise
          //Girişe Yönlendir
          //Başarılı ise Panele Yönlendir.
           session_start();
           $_SESSION['k_adi']=$_POST['k_adi'];
           $_SESSION['sifre']=$_POST['sifre'];
           header("Location:kontrolpanelx/kontrolnoktasi.php");
      }else {
          $hata = 1;
      }
      }
?>
<!doctype html>
<html lang="en">
  <?php include("view/tema/header.php"); //Head Tag'i ?>
  <style media="screen">
      <?php include("view/tema/style/admin_login.css"); ?>
  </style>
  <body>
    <div class="container-fluid">
  <?php include("view/tema/main_header.php"); //Logo / Giriş / Kayıt ?>
  <?php include("view/tema/top_navbar.php"); //Oteller / Pansiyonlar (Navbar menü)?>
    </div>

    <main role="main" class="container-fluid">
      <div class="row">
        <div class="col-lg-3 col-md-2"></div>
         <div class="col-lg-6 col-md-8 login-box">
             <div class="col-lg-12 login-key">
                 <i class="fa fa-key" aria-hidden="true"></i>
             </div>
             <div class="col-lg-12 login-title">
                 Yönetici Paneli
             </div>

             <div class="col-lg-12 login-form">
                 <div class="col-lg-12 login-form">
                     <form method="post" action="#">
                         <div class="form-group">
                             <label class="form-control-label">Kullanıcı Adı</label>
                             <input type="text" name="k_adi" class="form-control">
                         </div>
                         <div class="form-group">
                             <label class="form-control-label">Şifre</label>
                             <input type="password" name="sifre" class="form-control" i>
                         </div>

                         <div class="col-lg-12 loginbttm">
                             <div class="col-lg-6 login-btm login-text">
                                 <p style="display:<?php if ($hata == 1) {
                                   echo 'block';
                                 } else{
                                   echo 'none';
                                  }?>" class="text-danger">HATALI GİRİŞ YAPTIN</p>
                             </div>
                             <div class="col-lg-6 login-btm login-button">
                                 <button type="submit" class="btn btn-outline-primary">Giriş Yap</button>
                             </div>
                         </div>
                     </form>
                 </div>
             </div>
             <div class="col-lg-3 col-md-2"></div>

        <!-- /.main -->


      </div><!-- /.row -->

    </main><!-- /.container -->
          <!-- Footer -->
        <?php  include("view/tema/footer.php"); ?>
          <!--/Footer -->

    <!-- JavaScript -->
      <?php  include("view/tema/jscripts.php"); ?>
  <!--/JavaScript -->
  </body>
</html>
