<!doctype html>
<?php $urun = $vt->urun_sec(@$_GET['id']); ?>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <script src="https://use.fontawesome.com/2b18b16689.js"></script>
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
  <link href="https://fonts.googleapis.com/css?family=Stoke:300" rel="stylesheet">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

  <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
  <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>

    <title>Otel Bende</title>

    <link href="style/panel.css" rel="stylesheet">
    <style media="screen">
      label.file-upload{position:relative;overflow:hidden}label.file-upload input[type=file]{position:absolute;top:0;right:0;min-width:100%;min-height:100%;font-size:100px;text-align:right;filter:alpha(opacity=0);opacity:0;outline:0;background:#fff;cursor:inherit;display:block}
      .onizleme-on{
        display:block;
      }
      .onizleme-off{
        display: none;
      }
    </style>

  </head>

  <body>
    <nav class="navbar navbar-dark fixed-top bg-dark flex-md-nowrap p-0 shadow">
      <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="#">Otel Bende</a>

  <a class="navbar-brand col-sm-3 ml-auto col-md-2 mr-0" href="cikis.php">Çıkış Yap &nbsp<?php echo '('.$_SESSION["k_adi"].')'; ?></a>



    </nav>

    <div class="container-fluid">
      <div class="row">
      <?php include('sidebar.php'); ?>

        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-3">
          <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
            <h1 class="h2">Düzenle<small><p class="text-secondary"><?php date_default_timezone_set('Europe/Istanbul');
              echo date('d.m.Y');
             ?></p></small> </h1>
              <nav aria-label="breadcrumb">
                  <ol class="breadcrumb">
                      <li class="breadcrumb-item"><a href="panel">Panel</a></li>
                      <li class="breadcrumb-item"><a href="panel?s=duzenle&id=<?php echo $urun['id']; ?>">Düzenle</a></li>

                  </ol>
              </nav>

          </div>

          <script type="text/javascript">
  $(document).ready(function() {
      $('.file-upload').file_upload();
  });
</script>

<form class="form-horizontal" action="guncelle2.php" method="post" enctype="multipart/form-data">
  <div class="form-group row">
    <div class="col-md-6">
      <input class="form-control  form-control-lg" type="text" name="adi"  value="<?php echo $urun['adi']; ?>">
      <br>
      <label for="kat">Kategori</label>
      <select id="kat" name="kategori" class="form-control" >
        <?php
          $kat;
          switch ($urun['kategori']) {
            case 'Otel':
              $kat = "Otel";
              break;
            case 'Yurt':
              $kat = "Yurt";
              break;
            case 'Pansiyon':
              $kat = "Pansiyon";
              break;
            case 'Günlük Ev':
              $kat = "Günlük Ev";
              break;
            default:
              // code...
              break;
          }
        ?>
      <option <?php if ($kat == "Otel") {
        echo "selected";
      } ?> >Otel</option>
        <option  <?php if ($kat == "Pansiyon") {
          echo "selected";
        } ?> >Pansiyon</option>
          <option  <?php if ($kat == "Yurt") {
            echo "selected";
          } ?> >Yurt</option>
            <option  <?php if ($kat == "Günlük Ev") {
              echo "selected";
            } ?> >Günlük Ev</option>
      </select>


            <label for="kat">İl Seç</label>
          <select id="il" name="il" class="custom-select custom-select-lg mb-3" >
            <option><?php echo $urun['il'];?></option>
            <?php
              $vt->iller();
            ?>
          </select>

          <script>

                  //a.value şehir adıdır biz bunu il_no fonksiyonuna göndericez.
                  $(document).ready(function(){
                  $("#il").change(function(){

                          $.post("../controller/bolge.php",
                             {
                               il:$("#il").val(),
                               dada:"da"
                             },
                             function(data,status){
                                 $("#ilceler").html(data);
                                 console.log(status);
                             });
                      });
                  });
          </script>
            <label for="kat">İlçe Seç</label>
          <select id="ilceler" name="ilce" class="custom-select custom-select-lg custom-select-md mb-3" >
            <option><?php echo $urun['ilce'];?></option>

          </select>


    </div>
    <script type="text/javascript">
        $(document).ready(function() {
            $('.file-upload').file_upload();
        });
    </script>

        <div class="col-6 text-center bg-dark">
              <button type="button" class="btn btn-lg btn-danger" name="button"><a class="link text-white" href="panel.php?s=foto_guncelle&&id=<?php echo $urun['id']; ?>">Resimleri Güncellemek İstiyorsanız Tıklayın.</a></button>
              <img style="display:block" class="mx-auto" src="../view/img/logo.png" alt="">
        </div>
        <script>
        $(".fotolar").mouseenter(function() {
            $("#admin_onizleme").removeClass("onizleme-off");
            $("#admin_onizleme").addClass("onizleme-on");
            $("#admin_onizleme").attr("src",$(this).attr('src'));

        }).mouseleave(function () {
          $("#admin_onizleme").removeClass("onizleme-on");
          $("#admin_onizleme").addClass("onizleme-off");

        });

        </script>
  </div>
  <div class="row form-group">
    <div class="col-12 col-md-6">
      <!--Telefon 1-->
      <label for="tel1" class="col-2 col-form-label">Telefon 1</label>
      <div class="col-10">
      <input class="form-control" name="tel1" type="tel" placeholder="+90" value="<?php echo $urun['telefon_no_1'];?>" id="tel1" >
      </div>
        </div>
        <!--Telefon 2-->
        <div class="col-12 col-md-6">
          <label for="tel2" class="col-2 col-form-label">Telefon 2</label>
          <div class="col-10">
          <input class="form-control" name="tel2" type="tel" placeholder="+90" value="<?php echo $urun['telefon_no_2'];?>" id="tel2">
          </div>
        </div>
        <!--Websitesi-->
        <div class="col-12 col-md-6">
          <label for="tel1" class="col-12 col-md-6  col-form-label">websitesi</label>
          <div class="col-10">
          <input class="form-control" type="url" name="site" value="<?php echo $urun['websitesi'];?>" placeholder="www.siteadi.com" id="tel1">
          </div>
        </div>
        <!--Websitesi-->
  <div class="col-12 col-md-6">

        <label for="hizmet-saatleri">Hizmet Saatleri</label>
        <div class="col-12 col-md-6 row">
          <div class="input-group col-12 col-md-12">
          <div class="input-group-prepend">
            <span class="input-group-text" id="inputGroupPrepend">Kahvaltı</span>
          </div>
          <input type="text" class="form-control" name="kahvalti" id="kahvalti" value="<?php echo $urun['kahvalti'];?>" placeholder="örn:7:00-10:00" aria-describedby="inputGroupPrepend">
        </div>
        <div class="input-group col-12 col-md-12">
        <div class="input-group-prepend">
          <span class="input-group-text" id="inputGroupPrepend">Öğlen</span>
        </div>
        <input type="text" class="form-control" name="oglen" id="ogle" value="<?php echo $urun['ogle'];?>" placeholder="örn:11:00-15:00" aria-describedby="inputGroupPrepend">
      </div>
      <div class="input-group col-12 col-md-12">
      <div class="input-group-prepend">
        <span class="input-group-text" id="inputGroupPrepend">Akşam</span>
      </div>
      <input type="text" class="form-control" id="aksam" name="aksam" value="<?php echo $urun['aksam'];?>" placeholder="örn:18:00-20:00" aria-describedby="inputGroupPrepend">
    </div>
    <div class="input-group col-12 col-md-12">
    <div class="input-group-prepend">
      <span class="input-group-text" id="inputGroupPrepend">Gece(Açık Büfe)</span>
    </div>
    <input type="text" class="form-control" name="gece" id="gece" placeholder="örn:22:00-2:00" value="<?php echo $urun['gece'];?>" aria-describedby="inputGroupPrepend">
    </div>

      </div>

  </div>
  <div class="col-12 col-md-6">

      <div class="form-group">
            <label for="bitis" class="col-2 col-form-label">Bitiş Tarihi</label>
            <div class="col-10">
              <input class="form-control" name="bitis_tarihi" type="date" value="<?php echo $urun['bitis_tarihi'];?>"  id="bitis" >
            </div>
      </div>
  </div>
<div class="row">
  <div class="col-4">
    <span>Görünürlük</span>
    <input type="checkbox" name="gorunurluk" <?php if($urun['gorunurluk']=="on"){ echo "checked";} ?> data-toggle="toggle">
  </div>
  <div class="col-6">
    <label for="kat">Statü</label>
    <select id="kat" name="statu" class="form-control" required>

    <option <?php if($urun['statu']=="Normal"){ echo "selected";} ?>  >Normal</option>
    <option <?php if($urun['statu']=="Vitrin"){ echo "selected";} ?>>Vitrin</option>
    </select>
  </div>

</div>
<div class="row col-12">
  <div class="col-5">
      <div class="form-group">
        <label for="olanaklar">Olanaklar</label>
          <textarea name="olanaklar" id="olanaklar" placeholder="Buraya Kurumun olanakları yazılacak" class="form-control" rows="3" cols="10"><?php echo $urun['olanaklar'];?></textarea>
      </div>
  </div>
  <div class="col-5 offset-1">
    <label for="olanaklar">Yakın Merkezler</label>
      <textarea name="yakin_merkezler" id="olanaklar" placeholder="Yakın merkezler yazılacak" class="form-control" rows="3" cols="10"><?php echo $urun['yakin_merkezler'];?></textarea>
</div>
<div class="col-10 offset-1 ">
  <label for="olanaklar">Hakkında/Açıklama</label>
    <textarea name="hakkinda" id="hakkinda" placeholder="Hakkında Yazısı" class="form-control" rows="4" cols="10"><?php echo $urun['hakkinda'];?></textarea>
</div>
<br>
<div class="col-12 text-center">
  <br>
    <input type="text" style="display: none" name="id" value="<?php echo $urun['id']; ?>">
  <button type="submit" class="btn btn-success btn-md col-6" >Ekle</button>
</div>
</div>
</form>


        </main>


    </div>


    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
    <script src="https://getbootstrap.com/docs/4.1/assets/js/vendor/holder.min.js"></script>

    <!-- Icons -->
    <script src="https://unpkg.com/feather-icons/dist/feather.min.js"></script>
    <script>
      feather.replace()
    </script>

  </body>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
  <script src="https://getbootstrap.com/docs/4.1/assets/js/vendor/holder.min.js"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script type="text/javascript">
  +function ($) {
      'use strict';

      var FileUpload = function (element) {
          this.element = $(element);
          var defaultText = this.element.text();

          var label = this.element.text();
          var input = $('input', this.element);

          this.element.text('');
          this.element.append('<span class="file-upload-text"></span>');
          $('.file-upload-text', this.element).text(label);

          this.element.append(input);

          this.element.on('change', ':file', function() {
              var input = $(this);

              if (input.val()) {
                  var label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
                  var aa = label.substring(0,15);
                  $('.file-upload-text', $(this).parent('label')).text(aa+"...");
              }
              else {

                  $('.file-upload-text', $(this).parent('label')).text(defaultText);
              }
          });
      };

      function Plugin() {
          return this.each(function () {
              var $this = $(this);
              var data  = $this.data('bs.file-upload');

              if (!data) {
                  $this.data('bs.file_upload', (data = new FileUpload(this)));
              }
          });
      }

      var old = $.fn.file_upload;
      $.fn.file_upload = Plugin;
      $.fn.file_upload.Constructor = FileUpload;

      $.fn.file_upload.noConflict = function () {
          $.fn.file_upload = old;
          return this;
      };
  }(jQuery);

  </script>
    <script src="js/onizle.js"></script>
</html>
