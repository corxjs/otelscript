<?php
  $id = $_GET['id'];
  $foto = $_GET['foto'];
  $urun = $vt->urun_sec($id);
?>
<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <script src="https://use.fontawesome.com/2b18b16689.js"></script>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/css/bootstrap.min.css" integrity="sha384-Smlep5jCw/wG7hdkwQ/Z5nLIefveQRIY9nfy6xoR1uRYBtpZgI6339F5dgvm/e9B" crossorigin="anonymous">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
  <link href="https://fonts.googleapis.com/css?family=Stoke:300" rel="stylesheet">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <link href="style/ozel.css" rel="stylesheet">
  <title>Otel Bende</title>

  <link href="style/panel.css" rel="stylesheet">
  <style media="screen">
    .urun_resmi{
      height: auto;
      width: 350px;
      max-height: 400px;

    }
    .bg-corx{
      background: #f4f4f4;
      border:1px #acadaf solid;
      border-radius: 5%;
      max-height: 500px;
      max-width: 400px;
    }
  </style>
</head>

<body>
<nav class="navbar navbar-dark fixed-top bg-dark flex-md-nowrap p-0 shadow">
  <a class="navbar-brand col-sm-1 col-md-1 mr-0" href="#">Otel Bende</a>

  <a class="navbar-brand col-sm-1 ml-auto col-md-1 mr-0" href="cikis.php">Çıkış Yap &nbsp<?php echo '('.$_SESSION["k_adi"].')'; ?></a>

</nav>

<div class="container-fluid">
  <div class="row">


    <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
      <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Resim Güncelle (<?php echo $urun['adi'];?>)</h1>

      </div>


      <h2>Resim Güncelle</h2> <b>Güncelleme İşleminden Sonra Sekmeyi Kapatın.</b>
      <div class="row">
          <form action="#" method="post" enctype="multipart/form-data" id="pform">
        <div class="col-md-2 m-1 text-center align-middle bg-corx">
            <input type="text" id="id" name="id" style="display: none;" value="<?php echo $urun['id'];?>">
            <input type="text" id="foto" name="foto" style="display: none;" value="<?php echo $foto;?>">
            <input type="file" id="fotograf" onclick="onizle(this.name,this.id,'onizleme')" name="<?php echo $foto;?>" class="btn btn-success">
          <img class="urun_resmi img img-responsive"  src="../resimler/<?php echo $urun[$foto];?>" alt="">
            <button type="submit"  class="btn btn-info">Güncelle</button>
            <button type="button" id="sil"  class="btn btn-danger">Sil</button>
        </div>
          </form>
          <div style="display:block" class="col-md-2 m-1 onizleme text-center align-middle bg-corx">


          </div>
          <script>
                // *Burda Kaldık *//
              $("#pform").on("submit",function (e) {
                    e.stopPropagation();
                    e.preventDefault();
                    var data = new FormData(this);
                  $.ajax({
                      // Ajax isteği başlat
                      url:"guncelle.php", // yukle.php dosyasına istek gönder
                      data:data, // Veri olarak form data değişkeni içerisine depoladığımız değeleri gönder
                      processData: false,
                      contentType: false,
                      type:"POST", // İsteğimizin tipi post olsun
                      //dataType:"json", // Geriye json olarak değer göndersin
                      success:function(cevap)  // İstek başarılıysa
                      {
                         $('.durum').html(cevap);
                      }

                  })
              })
               $('#sil').click(function(){
                   var id = $('#id').attr("value");
                   var kolon = $('#foto').attr("value");
                   console.log(id);
                   console.log(kolon);
                   var istek = "sil";
                   $.ajax({
                       // Ajax isteği başlat
                       url:"guncelle.php", // yukle.php dosyasına istek gönder
                       data:{"id":id,"foto":kolon,"istek":"sil"}, // Veri olarak form data değişkeni içerisine depoladığımız değeleri gönder
                       type:"POST", // İsteğimizin tipi post olsun
                       success:function(cevap)  // İstek başarılıysa
                   {
                       $('.durum').html(cevap);
                   }

                     });
               });
                function onizle(isim,id,dc) {
                    console.log(isim);
                    console.log(id);
                    $("#"+id).change(function(){//eğer file değişirse
                        var resim=document.getElementById (id);//resime eriş
                        if (resim.files && resim.files[0]){//eğer dosya var ise
                            var veri=new FileReader();//veri okuma apisi başlatıyoruz.
                            veri.onload=function() {//altta readAsDataURL verileri okuyoruz.O okuma tamamladığın da
                                var adres=veri.result;//veriyi al
                                $('.'+dc).html('<img class="urun_resmi img img-responsive"  src="'+adres+'"/>');//resim olarak çizdir.
                            }
                            veri.readAsDataURL(resim.files[0]);//veri okuma
                        }
                    });
                }
          </script>
      </div>
        <div class="row">
            <div class="card">
                <div class="card-body durum">

                </div>
            </div>
        </div>
    </main>
  </div>

</div>



<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
<script src="https://getbootstrap.com/docs/4.1/assets/js/vendor/holder.min.js"></script>

<!-- Icons -->
<script src="https://unpkg.com/feather-icons/dist/feather.min.js"></script>
<script>
    feather.replace()
</script>


</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
</html>
