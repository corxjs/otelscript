<?php $urun = $vt->urun_sec(@$_GET['id']); ?>
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <script src="https://use.fontawesome.com/2b18b16689.js"></script>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/css/bootstrap.min.css" integrity="sha384-Smlep5jCw/wG7hdkwQ/Z5nLIefveQRIY9nfy6xoR1uRYBtpZgI6339F5dgvm/e9B" crossorigin="anonymous">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">

  <link href="https://fonts.googleapis.com/css?family=Stoke:300" rel="stylesheet">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <link href="style/ozel.css" rel="stylesheet">
    <title>Otel Bende</title>

    <link href="style/panel.css" rel="stylesheet">
    <style media="screen">
      .urun_resmi{
        height: auto;
        width: 350px;
        max-height: 400px;

      }
      .bg-corx{
        background: #f4f4f4;
        border:1px #acadaf solid;
        border-radius: 5%;
        max-height: 500px;
        max-width: 400px;
      }
    </style>
  </head>

  <body>
    <nav class="navbar navbar-dark fixed-top bg-dark flex-md-nowrap p-0 shadow">
      <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="#">Otel Bende</a>

  <a class="navbar-brand col-sm-3 ml-auto col-md-2 mr-0" href="cikis.php">Çıkış Yap &nbsp<?php echo '('.$_SESSION["k_adi"].')'; ?></a>

    </nav>

    <div class="container-fluid">
      <div class="row">
        <?php include('sidebar.php'); ?>

        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
          <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
            <h1 class="h2">Resim Güncelle (<?php echo $urun['adi'];?>)</h1>

            <div class="btn-toolbar mb-2 mb-md-0">
              <div class="btn-group mr-2">
                <button class="btn btn-sm btn-outline-secondary">Paylaş</button>
                <button class="btn btn-sm btn-outline-secondary">Dışarı Çıkar</button>
              </div>
              <button class="btn btn-sm btn-outline-secondary">
                <span data-feather="calendar"></span>
                Bu Ay
              </button>
              <button class="btn btn-sm btn-outline-secondary">
                <span data-feather="calendar"></span>
                Bu Yıl
              </button>
              <button class="btn btn-sm btn-outline-secondary">
                <span data-feather="calendar"></span>
                Bu Hafta
              </button>
            </div>

          </div>


          <h2>Ürün Resimleri</h2>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="panel">Panel</a></li>
                    <li class="breadcrumb-item"><a href="panel?s=duzenle&id=<?php echo $urun['id']; ?>">Düzenle</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Resim Güncelle</li>
                </ol>
            </nav>
          <div class="row">
            <div class="col-md-2 m-1 text-center align-middle bg-corx">
                <img class="urun_resmi img img-responsive" src="../resimler/<?php echo $urun['foto1'];?>" alt="">
                <a type="button" target="_blank" href="panel.php?s=foto_edit&id=<?php echo $urun['id'];?>&foto=foto1" class="btn btn-success col-md-12" name="button">Düzenle</a>
            </div>
              <div class="col-md-2 m-1 text-center align-middle bg-corx">
                  <img class="urun_resmi img img-responsive" src="../resimler/<?php echo $urun['foto2'];?>" alt="">
                  <a type="button" target="_blank" href="panel.php?s=foto_edit&id=<?php echo $urun['id'];?>&foto=foto2" class="btn btn-success col-md-12" name="button">Düzenle</a>
              </div>
              <div class="col-md-2 m-1 text-center align-middle bg-corx">
                  <img class="urun_resmi img img-responsive" src="../resimler/<?php echo $urun['foto3'];?>" alt="">
                  <a type="button" target="_blank" href="panel.php?s=foto_edit&id=<?php echo $urun['id'];?>&foto=foto3" class="btn btn-success col-md-12" name="button">Düzenle</a>
              </div>
              <div class="col-md-2 m-1 text-center align-middle bg-corx">
                  <img class="urun_resmi img img-responsive" src="../resimler/<?php echo $urun['foto4'];?>" alt="">
                  <a type="button" target="_blank" href="panel.php?s=foto_edit&id=<?php echo $urun['id'];?>&foto=foto4" class="btn btn-success col-md-12" name="button">Düzenle</a>
              </div>
              <div class="col-md-2 m-1 text-center align-middle bg-corx">
                  <img class="urun_resmi img img-responsive" src="../resimler/<?php echo $urun['foto5'];?>" alt="">
                  <a type="button" target="_blank" href="panel.php?s=foto_edit&id=<?php echo $urun['id'];?>&foto=foto5" class="btn btn-success col-md-12" name="button">Düzenle</a>
              </div>
              <div class="col-md-2 m-1 text-center align-middle bg-corx">
                  <img class="urun_resmi img img-responsive" src="../resimler/<?php echo $urun['foto6'];?>" alt="">
                  <a type="button" target="_blank" href="panel.php?s=foto_edit&id=<?php echo $urun['id'];?>&foto=foto6" class="btn btn-success col-md-12" name="button">Düzenle</a>
              </div>
              <div class="col-md-2 m-1 text-center align-middle bg-corx">
                  <img class="urun_resmi img img-responsive" src="../resimler/<?php echo $urun['foto7'];?>" alt="">
                  <a type="button" target="_blank" href="panel.php?s=foto_edit&id=<?php echo $urun['id'];?>&foto=foto7" class="btn btn-success col-md-12" name="button">Düzenle</a>
              </div>
              <div class="col-md-2 m-1 text-center align-middle bg-corx">
                  <img class="urun_resmi img img-responsive" src="../resimler/<?php echo $urun['foto8'];?>" alt="">
                  <a type="button" target="_blank" href="panel.php?s=foto_edit&id=<?php echo $urun['id'];?>&foto=foto8" class="btn btn-success col-md-12" name="button">Düzenle</a>
              </div>
          </div>
        </main>
      </div>
    </div>


    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
    <script src="https://getbootstrap.com/docs/4.1/assets/js/vendor/holder.min.js"></script>

    <!-- Icons -->
    <script src="https://unpkg.com/feather-icons/dist/feather.min.js"></script>
    <script>
      feather.replace()
    </script>

    <!-- Graphs -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js"></script>
    <script>
      var ctx = document.getElementById("myChart");
      var myChart = new Chart(ctx, {
        type: 'line',
        data: {
          labels: ["Pazar", "Pazartesi", "Salı", "Çarşamba", "Perşembe", "Cuma", "Cumartesi"],
          datasets: [{
            data: [0, 0, 0, 0, 23489, 0, 5],
            lineTension: 0,
            backgroundColor: 'transparent',
            borderColor: '#007bff',
            borderWidth: 4,
            pointBackgroundColor: '#007bff'
          }]
        },
        options: {
          scales: {
            yAxes: [{
              ticks: {
                beginAtZero: false
              }
            }]
          },
          legend: {
            display: false,
          }
        }
      });
    </script>
  </body>
</html>
