<nav class="col-md-2 d-none d-md-block bg-light sidebar">
  <div class="sidebar-sticky">
    <ul class="nav flex-column">
      <li class="nav-item">
        <a class="nav-link" href="panel.php">
          <span data-feather="circle"></span>
          Yönetim Paneli <span class="sr-only">(current)</span>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="?s=ekle">
          <span data-feather="circle"></span>
          Ekle
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="?s=ayarlar">
          <span data-feather="circle"></span>
          AnaSayfa Slayt
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="?s=yorumlar">
          <span data-feather="circle"></span>
          Yorumlar
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">
          <span data-feather="circle"></span>
          Tedarikçiler
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">
          <span data-feather="circle"></span>
          Extra Reklamlar
        </a>
      </li>
    </ul>

    <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
      <span>Raporlar</span>
      <a class="d-flex align-items-center text-muted" href="#">
        <span data-feather="plus-circle"></span>
      </a>
    </h6>
    <ul class="nav flex-column mb-2">
      <li class="nav-item">
        <a class="nav-link" href="#">
          <span data-feather="file-text"></span>
          Bu ay
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">
          <span data-feather="file-text"></span>
          Son Çeyrek
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">
          <span data-feather="file-text"></span>
          Sosyal Etkileşim
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">
          <span data-feather="file-text"></span>
          Yıl Sonu
        </a>
      </li>

    </ul>

  </div>
</nav>
