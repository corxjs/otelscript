<!doctype html>
<?php
  //include("../model/fonksiyonlar.php")

?>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <script src="https://use.fontawesome.com/2b18b16689.js"></script>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/css/bootstrap.min.css" integrity="sha384-Smlep5jCw/wG7hdkwQ/Z5nLIefveQRIY9nfy6xoR1uRYBtpZgI6339F5dgvm/e9B" crossorigin="anonymous">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">

  <link href="https://fonts.googleapis.com/css?family=Stoke:300" rel="stylesheet">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <link href="style/ozel.css" rel="stylesheet">
    <title>Otel Bende</title>

    <link href="style/panel.css" rel="stylesheet">
  </head>

  <body>
    <nav class="navbar navbar-dark fixed-top bg-dark flex-md-nowrap p-0 shadow">
      <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="#">Otel Bende</a>

  <a class="navbar-brand col-sm-3 ml-auto col-md-2 mr-0" href="cikis.php">Çıkış Yap &nbsp<?php echo '('.$_SESSION["k_adi"].')'; ?></a>

    </nav>

    <div class="container-fluid">
      <div class="row">
    <?php include("sidebar.php");?>

        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
          <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
            <h1 class="h2">Yorumlar</h1>

          </div>
          <div class="row col-12">
            <ul class="col-md-12 commnetList">
            <?php
              $vt->adminYorumCek();
            ?>
          </ul>
          <script>
            function onayla (id) {
                var yorum_id = id;
                var islem = "onay";
                $.ajax({
                  type: 'get',
                    url: 'yorumIslem.php',
                    data:{id:yorum_id,islem:islem},
                    success: function(result) {
                      if(result=="1"){
                          $("#"+yorum_id).remove();
                      }
                    }
                })
                console.log("Get Edildi.");
            }
            function sil (id) {
                var yorum_id = id;
                var islem = "sil";
                $.ajax({
                  type: 'get',
                    url: 'yorumIslem.php',
                    data:{id:yorum_id,islem:islem},
                    success: function(result) {
                        if(result=="1"){
                            $("#"+yorum_id).remove();
                        }
                    }
                })
                console.log("Get Edildi.");
            }
          </script>
          </div>
        </main>
      </div>
    </div>


    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
    <script src="https://getbootstrap.com/docs/4.1/assets/js/vendor/holder.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <!-- Icons -->
    <script src="https://unpkg.com/feather-icons/dist/feather.min.js"></script>
    <script>
      feather.replace()
    </script>

  </body>
</html>
