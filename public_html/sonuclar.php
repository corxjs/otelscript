<?php

include ("model/fonksiyonlar.php");
include ("view/tema/header.php");
include ('view/tema/top_navbar.php');

$ad = @$_GET['ad'];
$kategori = @$_GET['kategori'];
$il = @$_GET['il'];
$ilce = @$_GET['ilce'];
$sayfa=@$_GET['s'];
if (empty($sayfa)){
    $sayfa = 1;
}
$kacar = 5;
$ksayisi = $vt->kayit_sayisi();
$ssayisi = ceil($ksayisi/$kacar);
$nereden = ($sayfa*$kacar)-$kacar;
?>
    <main role="main" class="container-fluid">
    <div class="row">
    <!--Sol Sidebar -->


    <!--/.Sol Sidebar -->
    <div class="col-md-12 h-20 blog-main">
    <!--Slayt -->
    <div class="row">
        <div class="col-md-3 col-sm-12 left bg-warning">

            <?php include("view/tema/otel_arama.php"); //Arama Modülü?>
            <?php include("view/tema/metin_slogan.php"); ?>
            <?php // include("view/tema/kiralik_ilan.php"); //Kiralık İlan ?>
            </br>
            <?php // include("view/tema/tedarikciler.php"); //Kiralık İlan ?>
        </div>
        <div class="col-md-8 col-lg-9 col-sm-12">
           <?php $sonuc = $vt->Arama($ad,$kategori,$il,$ilce,$nereden,$kacar); ?>
           <nav aria-label="...">
             <div class="col-md-12 col-12 text-center">
               <ul class="pagination pagination-sm">

                 <?php
                 for ($i=1; $i <=$ssayisi ; $i++) {
                   echo '<li class="page-item"><a href="?ad='.$ad.'&kategori='.$kategori.'&il='.$il.'&ilce='.$ilce.'&s='.$i.'" ';
                   if ($i == $sayfa) {
                     echo 'class="page-link text-dark disabled">'.$i.'</a></li>';
                   }else{
                   echo ' class="page-link">'.$i.'</a></li>';
                 }
                 }

                 ?>
               </ul>
             </div>

</nav>
        </div>

    </div>
    </div>
    </div>
    </main>

<?php



include ("view/tema/footer.php");
include ("view/tema/jscripts.php");
?>
