<?php
  include("model/fonksiyonlar.php");

      //https://bootsnipp.com/similar/2XX5 bunu dene
      if (empty($_GET['id'])) {
        header("Location:index.php");
      }else{
        $id = $_GET['id'];
        $urun = $vt->urun_sec($id);
      }
?>
<!doctype html>
<html lang="en">
  <?php include("view/tema/header.php"); //Head Tag'i ?>
  <style media="screen">
  /* Smartphones (portrait and landscape) ----------- */
  @media only screen
  and (min-device-width : 320px)
  and (max-device-width : 480px) {
  /* Styles */
  .sl{
    height: 200px;
  }
  }
  @media
  only screen and (-webkit-min-device-pixel-ratio : 1.5),
  only screen and (min-device-pixel-ratio : 1.5) {
    .sl{
      height: 200px;
    }
  }
  /* Smartphones (landscape) ----------- */
  @media only screen
  and (min-width : 321px) {
    .sl{
      height: 200px;
    }
  }

  /* Smartphones (portrait) ----------- */
  @media only screen
  and (max-width : 320px) {
    .sl{
      height: 200px;
    }
  }
  /* Desktops and laptops ----------- */
  @media only screen
  and (min-width : 1224px) {
    .sl{
      height: 400px;
    }
  }
    .carousel-control-prev-icon, .carousel-control-next-icon {
        height: 20px;
        width: 20px;
        outline: black;
        background-color: rgba(0, 0, 0, 0.3);
        background-size: 50%, 50%;
    }
    .carousel-control-next-icon:hover{
    height: 60px;
    width: 60px;
    }
    .carousel-control-prev-icon:hover{
    height: 60px;
    width: 60px;
    }
    @import url(//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css);

.detailBox {

    border:1px solid #bbb;

}
.titleBox {
    background-color:#fdfdfd;
    padding:10px;
}
.titleBox label{
  color:#444;
  margin:0;
  display:inline-block;
}

.commentBox {
    padding:10px;
    border-top:1px dotted #bbb;
}
.commentBox .form-group:first-child, .actionBox .form-group:first-child {
    width:100%;
}
.commentBox .form-group:nth-child(2), .actionBox .form-group:nth-child(2) {
    width:100%;
}
.actionBox .form-group * {
    width:100%;
}
.taskDescription {
    margin-top:10px 0;
}
.commentList {
    padding:0;
    list-style:none;
    max-height:200px;
    overflow:auto;
}
.commentList li {
    margin:0;
    margin-top:10px;
}
.commentList li > div {
    display:table-cell;
}
.commenterImage {
    width:50px;
    margin-right:5px;
    height:100%;
    float:left;
}
.commenterImage img {
    width:100%;
    border-radius:50%;
}
.commentText p {
    margin:0;
}
.sub-text {
    color:#aaa;
    font-family:verdana;
    font-size:11px;
}
.actionBox {
    border-top:1px dotted #bbb;
    padding:10px;
}
.kat{
  font-size:20px;
}
  </style>

  <body>
    <div class="container-fluid">
  <?php // include("view/tema/main_header.php"); //Logo / Giriş / Kayıt ?>
  <?php include("view/tema/top_navbar.php"); //Oteller / Pansiyonlar (Navbar menü)?>
    </div>

    <main role="main" class="container-fluid">
      <div class="row">
        <!--Sol Sidebar -->


        <!--/.Sol Sidebar -->
        <div class="col-md-12 h-20 blog-main">
              <!--Slayt -->
              <div class="container-fluid">
              <div class="row">
                <div class="col-md-2 col-sm-12 left bg-warning">

                          <?php include("view/tema/otel_arama.php"); //Arama Modülü?>
                          <?php include("view/tema/metin_slogan.php"); ?>
                          <?php // include("view/tema/kiralik_ilan.php"); //Kiralık İlan ?>
                          </br>
                          <?php // include("view/tema/tedarikciler.php"); //Kiralık İlan ?>
                </div>


                    <div class="row col-md-10">


                                        <!--Carousel Wrapper-->
                                            <div id="carousel-example-1z" class="carousel slide carousel-fade col-12 col-md-6" data-ride="carousel">
                                                <h2 class="my-2"><?php echo $urun['adi']; ?></h2>
                                              <!--Indicators-->
                                              <ol class="carousel-indicators">
                                                <?php $vt->resimSayisi($urun['id']); ?>
                                              </ol>
                                              <!--/.Indicators-->
                                              <!--Slides-->
                                              <div class="carousel-inner "  role="listbox">
                                                  <!--First slide-->
                                                  <?php $vt->urun_foto($urun['id']); ?>
                                                  <!--/Third slide-->
                                              </div>
                                              <!--/.Slides-->
                                              <!--Controls-->
                                              <a class="carousel-control-prev " href="#carousel-example-1z" role="button" data-slide="prev">
                                                  <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                                  <span class="sr-only">Previous</span>
                                              </a>
                                              <a class="carousel-control-next" href="#carousel-example-1z" role="button" data-slide="next">
                                                  <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                                  <span class="sr-only">Next</span>
                                              </a>
                                              <!--/.Controls-->
                                            </div>
                                            <!--/.Carousel Wrapper-->
                                            <div class="col-md-6 col-12 ">
                                              <div class="row my-5">
                                              <div class="col-md-12">

                                                <p class="kat" ><b>Tip:</b><?php echo $urun['kategori']; ?></p>
                                                <p class="kat"><b>Konum:</b><?php echo $urun['il']."/".$urun['ilce']; ?></p>
                                                <p class="kat"><b>Olanaklar:</b><?php echo $urun['olanaklar']; ?></p>
                                                <p class="kat"><b>Yakın Merkezler:</b><?php echo $urun['yakin_merkezler']; ?></p>
                                                <p class="kat"><b>Web Sitesi:</b><?php echo $urun['websitesi']; ?></p>
                                                <p class="kat"><b>İrtibat:</b><?php echo $urun['telefon_no_1']; ?></p>
                                                <p class="kat"><b>Fiyat Aralığı :</b> <b><?php if(empty($urun['telefon_no_2'])){echo "--";}else{ echo $urun['telefon_no_2']."</b>₺'den başlayan fiyatlarla'"; }?></p>
                                               <a href="tel:<?php echo $urun['telefon_no_1']; ?>" type="button" class="btn col-12 btn-success  ">Hemen Ara</a>
                                              </div>

                                            </div>

                                            </div>
                                            <div class="col-md-12 col-12  my-5 rounded">
                                              <h2><?php echo $urun['kategori']; ?> Hakkında</h2>
                                              <p><?php echo $urun['hakkinda']; ?></p>
                                            </div>


                    </div>





              </div>

              <br>
              <style media="screen">
              .oneri{
                height: 1000px;
                display: block;
              }
              .oneri-inner{
                height: auto;
              }
              </style>
                <div class="row">
                  <div class="card-group col-12 col-md-10 offset-md-1">
                <?php $vt->urunOneri($urun['il']); ?>
                  </div>
                </div>


                  <div class="col-md-12 col-12  rounded">

                    <div class="detailBox col-12">
                    <div class="titleBox">
                    <label>Yorumlar</label>
                    </div>
                    <div class="commentBox">

                    <p class="taskDescription">Burası Hakkındaki yorumlarınızı bizimle paylaşın</p>
                    </div>
                    <div class="actionBox">
                    <ul class="commentList">
                      <?php $vt->yorum_cek($urun['id']); ?>
                    </ul>
                    <hr>
                    <div class="form-inline" role="form">
                    <div class="form-group col-md-6 col-12">
                    <input class="form-control col-10 my-1" name="ad" id="ad" type="text" placeholder="Adınız"  required />
                    <input class="form-control col-10 my-1" name="soyad" type="text" placeholder="Soyadınız" required />
                    <input class="form-control col-10 my-1" name="eposta" type="text" placeholder="Eposta Adresiniz"  required/>
                    <br>
                    <input class="form-control col-12 my-1" name="yorum" type="text" placeholder="Yorumunuz" required />
                    </div>
                    <div class="col-12">
                      <div class="sonuc">

                      </div>
                    <button  class="btn btn-lg col-12 yorum_yap">Gönder</button>
                    </div>
                    <input type="text" name="id" style="display:none" value="<?php echo $urun['id']; ?>">
                  </div>
                    </div>
                    </div>
                      <script>
                        $(".yorum_yap").click(function() {

                          var ad = $("#ad").val();
                          var soyad = $("input[name=soyad]").val();
                          var eposta = $("input[name=eposta]").val();
                          var yorum = $("input[name=yorum]").val();
                          var id = $("input[name=id]").val();
                          if (ad == "" || soyad == "" || eposta == "" || yorum == "") {
                              $('.sonuc').html('<p class="bg-danger col-md-4 col-12 text-white text-center offset-md-4">Eksik Alanları Doldurun</p>');
                              console.log(ad+soyad+eposta+yorum);
                          }else{
                            console.log(ad);
                          $.ajax({

                                      type: 'GET',
                                      url: 'controller/yorum.php',
                                       data: {"ad":ad,"soyad":soyad,"eposta":eposta,"yorum":yorum,"id":id},
                                      success: function(sonuc) {
                                          $('.sonuc').html(sonuc);
                                      },
                                    });
                                  }
                        });


                      </script>
                  </div>

            <!--/.Slayt-->
<div class="row">




</div>

        <!-- /.main -->
        <aside class="col-md-3 blog-sidebar ">



          <?php //  include("view/tema/sehir_otel_liste.php"); ?>


        </aside><!-- /.blog-sidebar -->

      <!-- /.row -->
          <div class="row">
            <?php include("view/tema/tanitim_jumbotron.php"); ?>
            <?php  include("view/tema/sosyal_medya.php"); ?>
          </div>
    </main><!-- /.container -->
          <!-- Footer -->
        <?php  include("view/tema/footer.php"); ?>
          <!--/Footer -->

    <!-- JavaScript -->
      <?php  include("view/tema/jscripts.php"); ?>
  <!--/JavaScript -->
  </body>
  <script>
  (function($) {
  "use strict";

  // manual carousel controls
  $('.next').click(function(){ $('.carousel').carousel('next');return false; });
  $('.prev').click(function(){ $('.carousel').carousel('prev');return false; });

})(jQuery);
  </script>

</html>
