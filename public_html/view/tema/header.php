<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<script src="https://use.fontawesome.com/2b18b16689.js"></script>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
  <link rel="stylesheet" href="style/kral.css" >
<link href="https://fonts.googleapis.com/css?family=Stoke:300" rel="stylesheet">
<link href="view/tema/style/navbar-style.css" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<style media="screen">
  .katfoto{
    height: 150px;
    width: 368px;
  }
  .logoyazi{
    font-size:30px;
  }
  @media only screen and (max-width: 600px) {
    .logoyazi {
        display:none;
    }

}
@media only screen
and (min-device-width : 320px)
and (max-device-width : 480px) {
/* Styles */
.asl{
  height: 200px;
}
}
@media
only screen and (-webkit-min-device-pixel-ratio : 1.5),
only screen and (min-device-pixel-ratio : 1.5) {
  .asl{
    height: 200px;
  }
}
/* Smartphones (landscape) ----------- */
@media only screen
and (min-width : 321px) {
  .asl{
    height: 200px;
  }
}

/* Smartphones (portrait) ----------- */
@media only screen
and (max-width : 320px) {
  .asl{
    height: 200px;
  }
}
/* Desktops and laptops ----------- */
@media only screen
and (min-width : 1224px) {
  .asl{
    height: 600px;
  }
}

</style>
  <title>Otel Bende</title>

</head>
