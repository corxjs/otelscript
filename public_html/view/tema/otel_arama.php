<div style="" class="p-3 mb-1 my-2 bg-light rounded">
  <h4 class="font-italic">Arama Yap</h4>
  <p class="mb-0" style="font-size:15px;">Otel, Pansiyon, Yurt veya Günlük Ev Ara</p>


  <form action="sonuclar.php" method="get">
    <div class="form-group">
      <input type="text" name="ad" class="form-control" id="exampleFormControlInput1" placeholder="Otel & Pansiyon Adı İle Ara" >
    </div>
    <select name="kategori"  class="custom-select custom-select-lg mb-3">
    <option value="Tümü">Tümü</option>
    <option value="Otel">Otel</option>
    <option value="Pansiyon">Pansiyon</option>
    <option value="Yurt">Yurt</option>
    <option value="Günlük Ev">Günlük Ev</option>
    </select>
      <p>İllere Göre Ara</p>
    <select name="il" id="il"  class="custom-select custom-select-lg mb-3">
      <option>İstanbul</option>
        <option>Tümü</option>
      <?php
        $vt->iller();
      ?>
    </select>
    <script>

            //a.value şehir adıdır biz bunu il_no fonksiyonuna göndericez.
            $(document).ready(function(){
                $.post("controller/bolge.php",
                    {
                        il:$("#il").val(),
                        dada:"da"
                    },
                    function(data,status){
                        $("#ilceler").html(data);
                        console.log(status);
                    });
            $("#il").change(function(){

                    $.post("controller/bolge.php",
                       {
                         il:$("#il").val(),
                         dada:"da"
                       },
                       function(data,status){
                           $("#ilceler").html(data);
                           console.log(status);
                       });
                });
            });
    </script>
    <select  id="ilceler" name="ilce" class="custom-select custom-select-lg custom-select-md mb-3">


    </select>
    <button type="submit" class="btn btn-info col-12">Ara</button>
  </form>
</div>
