<div class="jumbotron bg-light text-dark col-12 col-md-9 col-lg-9">
  
  <p class="display-5 text-info"><img src="view/img/logo.png" width="100" height="70"><span class=" my-3" style="font-size:30px" >otelbende.com</span></p>

  <p class="lead text-dark mx-3">Otel, pansiyon, yurt ve günlük ev arıyorsanız doğru yerdesiniz. İster ilan verin ister kendinize uygun otel, pansiyon, yurt ve günlük kiralık  evinizi arayın.</p>
  <hr class="my-4 text-dark">
  <p class="text-dark mx-3">Bizimle iletişim kurarak tanıtımızı yayınlatabilirsiniz.</p>
  <a class="btn btn-warning text-dark btn-lg mx-3" href="?sayfa=iletisim" role="button">İletişim</a>
  <a class="btn btn-warning text-dark btn-lg mx-3" href="?sayfa=hakkimizda" role="button">Hakkımızda</a>
</div>
