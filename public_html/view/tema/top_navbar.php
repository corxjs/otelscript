<nav class="navbar navbar-expand-lg  fixed-top navbar-dark  " style="background:#f2f3f4">

    <div class="row  col-md-12 col-xs-12 col-lg-6 col-sm-12 col-12 align-items-center">

      <div class="col-3 col-md-6 text-center ">
          <a href="index.php">
        <img src="view/img/logo.png"  class="blog-header-logo img-fluid pull-left " href="#"></img>

          </a>
          <a class="text-info pull-right logoyazi"  href="index.php">
              &nbspotelbende.com
          </a>
      </div>
      <div class=" text-info col-3 offset-6 px-1 d-flex pull-right justify-content-end text-white align-items-center">
        <button class="navbar-toggler text-info bg-info   border-0" style="position:relative;left:10%" type="button" data-toggle="offcanvas">
          <span class=" text-info navbar-toggler-icon"></span>
        </button>
      </div>

    </div>



      <div class="navbar-collapse  offcanvas-collapse" style="background:#f2f3f4" id="navbarsExampleDefault">
        <ul class="navbar-nav ">
          <li class="nav-item">
            <a class="text-info nav-link d-sm-none d-none d-md-block d-lg-block" href="index.php"><i class="fa fa-home">Anasayfa </i></a>
          </li>

          <li class="nav-item">
            <a class="text-info nav-link d-sm-none d-none d-md-block d-lg-block" href="#"><i class=""> |  </i></a>
          </li>

          <li class="nav-item active">
          <a href="?sayfa=oteller" class="nav-link text-info"> <i class="fas fa-building"></i>&nbsp Otel</a>
          </li>
          <li class="nav-item">
             <a class="text-info nav-link" href="?sayfa=pansiyonlar"><i class="far fa-building fa-1x"></i>&nbsp Pansiyon</a>
          </li>
            <li class="nav-item">
                <a class="text-info nav-link" href="?sayfa=yurtlar"><i class="far fa-building fa-1x"></i>&nbsp Yurt</a>
            </li>
          <li class="nav-item">
            <a class="text-info nav-link" href="?sayfa=gunlukevler"><i class="fas fa-home fa-1x"></i>&nbsp Günlük Ev</a>
          </li>
          <li class="nav-item">
            <a class="text-info nav-link d-sm-none d-none d-md-block d-lg-block" href="#"><i class=""> |  </i></a>
          </li>
          <li class="nav-item">
            <a class="text-info nav-link" href="?sayfa=iletisim"><i class="fas fa-map-marker"></i>&nbsp İlan Ver</a>
          </li>

        </ul>
      </div>
    </nav>

    <div class="nav-scroller text-info  box-shadow d-sm-block d-block d-md-none d-lg-none">
      <nav class="nav nav-underline">
        <a class="nav-link text-info active" href="#">Anasayfa</a>
        <a class="nav-link text-info" href="?sayfa=oteller"><i class="fas fa-building"></i>&nbsp Otel</a>
        <a class="nav-link text-info" href="?sayfa=pansiyonlar"><i class="far fa-building fa-1x"></i>&nbsp Pansiyon</a>
        <a class="nav-link text-info" href="?sayfa=yurtlar"><i class="fas fa-building fa-1x"></i>&nbsp Yurt</a>
        <a class="nav-link text-info" href="?sayfa=gunlukevler"><i class="fas fa-home fa-1x"></i>&nbsp Günlük Ev</a>
        <a class="nav-link text-info" href="#"><i class="far fa-handshake"></i>&nbsp Hakkımızda</a>
        <a class="nav-link text-info" href="#"><i class="fas fa-map-marker"></i>&nbsp İlan Ver</a>

      </nav>
    </div>
